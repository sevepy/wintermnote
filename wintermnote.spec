# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['C:/Users/admin/workplace/wintermnote/wintermnote.py'],
             pathex=['C:\\Users\\admin\\workplace\\wintermnote'],
             binaries=[],
             datas=[('C:/Users/admin/workplace/wintermnote/desktop.ini', '.'), ('C:/Users/admin/workplace/wintermnote/wintermnote.ico', '.'), ('C:/Users/admin/workplace/wintermnote/wintermnote.ini', '.'), ('C:/Users/admin/workplace/wintermnote/wintermnote.exe', '.'), ('C:/Users/admin/workplace/wintermnote/.alacritty.yml', '.'), ('C:/Users/admin/workplace/wintermnote/alacritty.exe', '.')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=['scipy', 'zope', 'PIL'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='wintermnote',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True , icon='C:\\Users\\admin\\workplace\\wintermnote\\iconified\\favicon.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='wintermnote')
