# Wintermnote-plugin for opening URL's in a web-browser
##  Copyright (C) 2013  Jonas Møller <jonasmo441@gmail.com>
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.
## ===========================================================================

## Finds urls on the current text and opens them (let's user choose, if there are several)
## The following styles are permitted:
## protocol://domain.extension
## protocol://www.domain.extension
## protocol://www.subdomain.domain.extension
## www.domain.extension
##
## If email addresses are found, mailto:// will be assumed
##
## Permitted protocols:
## http(s)://
## ftp(s)://
## mailto://
## file://

## import pdb
import pyperclip
from pathlib import Path
import re, webbrowser
from subprocess import call
from rich.console import Console
from rich.prompt import IntPrompt
## Regexes
rx_http_link = re.compile(r"https?://[a-zA-Z0-9\./_%?&#=-]*")
rx_www_link = re.compile(r"www\.[a-zA-Z0-9\./_%?&#=-]*")
#rx_ftp_link = re.compile(r"ftps?://[a-zA-Z0-9\./_%?&#=-]*")
rx_sftp_link = re.compile(r"sftp?://[\w.-]+@[\w.-]+.\w+/?.*")
## the browser
# rx_file_link = re.compile(r"file?:\/\/\/\w:(\\\\[A-Za-z_\-\s0-9\.]+)+\.PNG|gif|png|bmp|gif|jpg|jpeg")
rx_file_link = re.compile(r"file?:\/\/\w:.*\.PNG|file?:\/\/\w:.*\.gif|file?:\/\/\w:.*\.png|file?:\/\/\w:.*\.bmp|file?:\/\/\w:.*\.gif|file?:\/\/\w:.*\.jpg|file?:\/\/\w:.*\.jpeg|file?:\/\/\w:.*\.PDF|file?:\/\/\w:.*.pdf")

rx_cbthunderlink_link = re.compile(r"cbthunderlink:\/\/.*")

## TODO rsync
#rx_rsync_link = re.compile(r"sftps?://[a-zA-Z0-9\./_%?&#=-]*")
#rx_email_address = re.compile(r"\w*@\w*\.\w*")
rx_email_address = re.compile(r'[\w.-]+@[\w.-]+.\w+')
## TODO Doi protocol
rx_has_protocol = re.compile(r"[a-zA-Z]*://")
# https://codereview.stackexchange.com/questions/104480/a-four-item-text-based-menu-in-python

class Menu():
    SEPARATOR = '-'
    _title = ''

    def __init__(self, title, options):
        self._title = title
        self._options = options
        self.console = Console()

    def header(self, text):
        line = self.SEPARATOR * self.console.size.width
        return f"\n{line}\n {text}\n{line}\n"

    def display(self):
        string = self.header(self._title)
        for soption in self._options:
            string += f"        ({soption[0]})      [yellow]{soption[1]}[/]\n"
        self.console.print(string)

    def prompt(self,choices=0):
        if choices == 0:
            return choices
        else:
            select_from = ["0"]
            for linknumber in range(choices):
                select_from.append(str(linknumber+1))

        v = IntPrompt.ask("Press 0 to quit - ",choices=select_from,default=0)
        return v

def modURL(url):
    if not rx_has_protocol.match(url):
        return "https://" + url
    elif url.startswith("file:"):
        try:
            return url.replace("\\\\","\\")
        except:
            return r'{}'.format(url)
    elif url.startswith("cbthunderlink:"):
        pyperclip.copy(url)
        try:
            open_cbthunderlink = str(Path(__file__).parent.parent) + "\\open_cbthunderlink.exe"
            call([open_cbthunderlink])
        except:
            return None
        return None

    return url

## Get all the matches
class OpenProtocol(object):

    def __init__(self,text:str):
        self.urls = rx_http_link.findall(text)
        self.urls.extend([url for url in rx_www_link.findall(text) if modURL(url) not in self.urls])
        #self.urls.extend(rx_ftp_link.findall(text))
        sftp_links = rx_sftp_link.findall(text)
        self.urls.extend(sftp_links)
        rx_email_filterd = [ e for e in rx_email_address.findall(text) if not " ".join(sftp_links).find(e) > -1 ]
        self.urls.extend(map(lambda x: "mailto://"+x, rx_email_filterd))
        # pdb.set_trace()
        # weak solution
        self.urls.extend([file for file in rx_file_link.findall(r'{}'.format(text)) if Path(file[7:]).is_file()])

        self.urls.extend([cblink for cblink in rx_cbthunderlink_link.findall(r'{}'.format(text)) ])
    def select(self):
        SFTP = False
        sftpexe = str(Path(__file__).parent.parent) + "\\wintermnote_winscp.exe"

        links_options =[]
        links_options.append((0,'Quit and return to main menu'))
        links_options = [lo for lo in enumerate(self.urls,start=1)]
        main_menu = Menu("Please Select an Option",links_options)
        main_menu.display()
        choice = main_menu.prompt(len(self.urls))
        if choice == 0:
            return
        else:
            choice = choice-1
            if choice < len(self.urls):
                if (self.urls[choice]).startswith("sftp"):
                    SFTP = True

                url = modURL(self.urls[choice])
                if SFTP == False:
                    if url!=None:
                        webbrowser.open(url)
                        print(f"\r\nOpening  {url}")
                else:
                    cmd = f" \"{url}\""
                    call([sftpexe,cmd])
            elif len(self.urls) == 1:
                # pdb.set_trace()
                if self.urls[0].startswith("sftp"):
                    cmd = f" \"{url}\""
                    call([sftpexe,cmd])
                else:
                    print(f"OpenURL: Opening {self.urls[0]}")
                    webbrowser.open(self.urls[0])
            else:
                print('OpenURL: No links found.')

