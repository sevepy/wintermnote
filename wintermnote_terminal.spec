# -*- mode: python ; coding: utf-8 -*-


a = Analysis(
    ['C:/Users/Seve/workplace/wintermnote/wintermnote_terminal.py'],
    pathex=[],
    binaries=[],
    datas=[('C:/Users/Seve/workplace/wintermnote/desktop.ini', '.'), ('C:/Users/Seve/workplace/wintermnote/open_cbthunderlink.exe', '.'), ('C:/Users/Seve/workplace/wintermnote/wintermnote.bat', '.'), ('C:/Users/Seve/workplace/wintermnote/wintermnote.ico', '.'), ('C:/Users/Seve/workplace/wintermnote/wintermnote.ini', '.'), ('C:/Users/Seve/workplace/wintermnote/vimrc', '.'), ('C:/Users/Seve/workplace/wintermnote/wintermnote_winscp.exe', '.'), ('C:/Users/Seve/workplace/wintermnote/wintermnote.exe', '.'), ('C:/Users/Seve/workplace/wintermnote/.alacritty.yml', '.'), ('C:/Users/Seve/workplace/wintermnote/alacritty.exe', '.'), ('C:/Users/Seve/workplace/wintermnote/bin', 'bin'), ('C:/Users/Seve/workplace/wintermnote/lib', 'lib')],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=['scipy', 'zope', 'PIL', 'matplotlib'],
    noarchive=False,
)
pyz = PYZ(a.pure)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='wintermnote_terminal',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    icon=['C:\\Users\\Seve\\workplace\\wintermnote\\iconified\\favicon.ico'],
)
coll = COLLECT(
    exe,
    a.binaries,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='wintermnote_terminal',
)
