#NoTrayIcon
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
; #Persistent

;^0:: ;ctrl-0
If WinExist("Mozilla Thunderbird")
{
  WinActivate, Mozilla Thunderbird
  Sleep, 500
}
Else
{
  Run "thunderbird.exe"
  WinActivate
  Sleep, 2000
}

Send ^!0
Return

ExitApp
