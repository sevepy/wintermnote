
;#SingleInstance Force
; exec wintermnote_terminal.exe
; startup script for wintermnote_terminal app
; version 0.0.9
; alacritty compiled from forked repository
;   - replaced ico
; unicode support
; author : @seve_py
terminal := GetConfig("UI","terminal","default")
if (terminal="default")
{
Run %ComSpec%  /Q /c "wintermnote_terminal.exe",,Max
} else if (terminal="alacritty") {
yml := " --config-file " . A_AppData . "\alacritty\alacritty.yml "
    if !FileExist(A_AppData . "\alacritty\alacritty.yml")
        yml := " --config-file " . A_ScriptDir . "\_internal\.alacritty.yml "
   
cl :=" " . A_ScriptDIr . "\_internal\alacritty.exe" . yml . " --command " . A_ScriptDir . "\_internal\wintermnote.bat"
Run %ComSpec% /Q /c %cl%,,Hide
} else if (terminal="wt") {
cl :=  "wt.exe -M -d " . A_ScriptDir . "\_internal wintermnote.bat"
Run %cl%,,Max
; Make Terminal Window Active Once Opened
	WinWait, ahk_exe WindowsTerminal.exe, , 3
	if ErrorLevel
		MsgBox, Couldn't Find Terminal
	else
		WinActivate

} else {
msgbox, Not a valid terminal, possible options are: default, alacritty, wt. 
Run %ComSpec%  /Q /c "wintermnote_terminal.exe",,Max
}
return


GetConfig(section, key, default) {
    IniRead value, %A_ScriptDir%\_internal\wintermnote.ini, %section%, %key%, %default%
    return value
}

