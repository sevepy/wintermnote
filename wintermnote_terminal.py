# -*-coding: utf-8 -*-

__metaclass__ = type
import re
import os
import sys
from datetime import date
##from rich import print
from notelib import NotesIndex
from notelib import OpenProtocol
from subprocess import call, run, CalledProcessError
from prompt_toolkit import prompt
from prompt_toolkit.completion import WordCompleter
from collections import OrderedDict
import configparser
from pathlib import Path
import shutil
from rich.console import Console
import pdb
import sqlite3
import pyperclip
from difflib import get_close_matches
import string
DEBUG = True
VERSION = 'forked from v1.1.6'
WINTERMNOTE_VERSION = '0.3.0'

class App:

    def __init__(self, editor = "notepad.exe", storage = "\\Desktop\\Notes",
            screen_width = 100,
            suffix = ".txt",
            lang="en_US",
            terminal="alacritty",
            backup="no",
            buffer=".scratch_buffer",
            ditto_db = "myclipboard.db",
            default_notes_file = "",
            servername = "" ,
            console = None,
            external = False):
        user_storage       = os.path.expanduser("~")+storage
        self.editor        = editor
        self.storage       = user_storage
        self._project_root = Path(__file__).parent
        self.suffix        = suffix
        self.columns       = screen_width
        self.lang          = lang
        self.terminal      = terminal
        self.backup        = backup
        self.index         = None
        self.buffer        = buffer
        self.ditto_db      = os.path.expanduser("~")+"\\"+ditto_db
        self.default_notes_file      = os.path.expanduser("~")+"\\"+default_notes_file
        self._servername     = servername
        self.console       = console
        self.external       = False

    def set_external(self, external):
        self.external = external
    
    def get_terminal(self):
        return self.terminal

    @property
    def servername(self):
        return self._servername

    @property
    def get_default_notes_file(self):
        return self.default_notes_file

    def get_ditto_db(self):
        return self.ditto_db

    def get_storage(self):
        return self.storage

    def get_editor(self):
        return self.editor

    def get_suffix(self):
        return self.suffix

    def get_columns(self):
        return self.columns

    def set_columns(self):
        try:
            self.columns = self.console.size.width
        except:
            self.columns = self.columns


    def set_index(self,index):
        self.index = index

    def get_buffer(self):
        return Path(Path.home(),self.buffer)

    def init_storage(self):

        if os.path.isdir(self.storage) == False:
            os.mkdir(self.storage) # create user notes storage if not exists
            source_conf = Path(self._project_root,"desktop.ini")
            dest_conf = Path(self.storage,"desktop.ini")
            shutil.copyfile(source_conf,dest_conf)

            # set attrib +S + H to desktop.ini
            call(["attrib","+S","+H",str(dest_conf)])
            ## copy icon in dest folder
            source_conf = Path(self._project_root,"wintermnote.ico")
            dest_conf = Path(self.storage,"wintermnote.ico")
            shutil.copyfile(source_conf,dest_conf)

            ## copy .vimrc should use set exrc in home .vimrc
            ## default netrw configuration
            ## local vimrc

            try:
                source_conf = Path(self._project_root,"vimrc")
                dest_conf = Path(self.storage,".vimrc")
                shutil.copyfile(source_conf,dest_conf)
            except:
                pass

            ## attrib parent
            call(["attrib","+r", str(self.storage)])




    @staticmethod
    def clr():
        """
        Keeps screen clean
        """
        os.system('cls' if os.name == 'nt' else 'clear')
        return

    def interact(self, qry, options=[], default=None, isafilename = True):
        """
        Interacts with user
        """
        qry = qry.decode() if hasattr(qry, 'decode') else qry
        if default is None:
            default = ''
        if hasattr(default, 'decode'): default = default.decode()
        options = list(map(str, options))
        completer = WordCompleter(options, ignore_case=True)

        ## interact with user
        inp = prompt(qry, completer=completer, default=default)

        if inp:
            if isafilename and inp.endswith(self.suffix) == False :
                return inp+self.suffix
            else:
                return inp
        else:
            return

    @staticmethod
    def format_filename(s):
        """Take a string and return a valid filename

    """
        valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
        filename = ''.join(c for c in s if c in valid_chars)
        # filename = filename.replace(' ','_') remove spaces in filenames.
        return filename

    def readfile(self,filename):
        """
        Reads file and returns its content
        """
        try:
            with open(os.path.join(self.storage, filename),encoding='utf-8') as f:
                content = f.read()
        except FileNotFoundError:
            content = "File not found."

        return content

    @staticmethod
    def match_word(word, content):
        docwords = re.split('[^a-zA-Z0-9]', content.lower())
        # fuzzy match
        #return len([x for x in docwords if x.startswith(word)])
        fm = get_close_matches(word,docwords,n=8,cutoff=0.7)
        return len(fm)

class TermNote:
    """
    Just the main class
    """
    def __init__(self,app):
        self.note = None
        self.searched = []
        self.found = OrderedDict()
        self.options = OrderedDict()
        self.allnotes = OrderedDict()
        self.allwords = []
        self.app = app
        self.console = Console()

    def new(self, title=None):
        app.clr()
        filename = self.app.interact('Enter title: ', default=title,
                            options=self.allwords, isafilename = True)
        filename = app.format_filename(filename)
        if filename:

            filepath = os.path.join(self.app.get_storage(), filename)

            while os.path.exists(filepath):
                app.clr()
                self.console.print('[orange]Note "{}" already exists![/]'.format(filename))
                filename = self.app.interact('Try another title: ',
                                    default=filename, options=self.allwords, isafilename = True)
                filepath = os.path.join(app.get_storage(), filename)

            with open(filepath,"w", encoding='utf-8') as newnote:
                ## TODO got to use a regular expr. here
                newnote.write(filename.replace(self.app.get_suffix(), "")+"\n\n") ## makes compatible with vim-note
                try:
                    with open(self.app.get_default_notes_file(),"r",encoding='utf8') as dnf:
                        newnote.write(dnf.read())
                except:
                    dnf ="""
{today}

{tags}

            * * * *

                          """
                    basename = filename.replace(self.app.get_suffix(),"").split(" ")
                    newnote.write(dnf.format(
                        tags ="\n".join(["@"+x for x in basename if len(x)>3]),
                        today = date.today())
                        )

            call([self.app.get_editor(), filepath])

            # redundant
            if os.path.isfile(filepath):
                self.note = filename
            else:
                pass

    def scandir(self):
        allfilenames = [f for f in os.listdir(self.app.get_storage()) if os.path.isfile(self.app.get_storage()+'/'+f) and f.endswith(".ini") == False and f.endswith(self.app.get_suffix()) == True]
        allcontents = [self.app.readfile(f) for f in allfilenames]
        self.allwords = []
        self.allnotes = OrderedDict()
        for x in allcontents + allfilenames:
            self.allwords.extend(re.split('[^a-zA-Z0-9]', x))
        self.allwords = list(set(self.allwords))
        if '' in self.allwords: 
            self.allwords.remove('')
        self.allnotes = OrderedDict(zip(allfilenames, allcontents))

    def search_or_select(self, entered=None):
        self.scandir()

        if entered is None:
            entered = self.app.interact(
                '\n Search or select: ',
                options=list(self.options.keys()) + list(self.found.keys()) + self.allwords,
                isafilename = False
            )
        # case CR entered = None
        # print(f"=== {entered}")
        if entered is None:
            return
        if re.sub('[0-9]', '', entered) == '' and int(entered) in self.found:
            self.note = self.found[int(entered)]
            return
        if entered in self.options:
            getattr(self, self.options[entered])()
            return
        self.searched = entered.split()
        match_count = {
            k: sum([self.app.match_word(w, k+' '+v) for w in self.searched])
                for k,v in self.allnotes.items()
        }
        found = [k for k,v in match_count.items() if v != 0]
        self.found = OrderedDict(enumerate(
            sorted(found, key=lambda x: match_count[x], reverse=True)
        ))
        if len(self.found) == 0:
            self.app.clr()
            print('No result found for "{}"...!\n'.format(entered))
            ans = self.app.interact('Create new note? [y/N] ', options=['y', 'n'], isafilename = False)
            if ans in ['y', 'Y']:
                self.new(entered)
        elif len(self.found) == 1:
            self.note = self.found[0]

    def display(self):
        self.app.clr()
        with self.console.pager(styles = True):
            if self.note is not None:
                ## set Tab title for wt
                try:
                    run(["TITLE",self.note],shell=True)
                except CalledProcessError as e:
                    pass

                filepath = self.app.get_storage() + '/' + self.note
                self.console.print('[bold yellow]'+self.note+'[/]',justify='center')
                title_and_note = self.app.readfile(filepath)
                sn = self.app.servername

                if sn.lower() != "none" :
                    if self.app.servername :
                        servername = self.app.servername

                    try:
                        ec = call([self.app.get_editor(),"--servername", servername, "--remote-silent",os.path.join(self.app.get_storage(), self.note)])
                    except:
                        ec = 1

                    if ec == 0:
                        self.console.print(f'[bold gray] vim server {servername} ',justify='center')
                    else:
                        self.console.print(f'[bold red] vim server error {ec} {servername} ',justify='center')
                try:
                    title, note = title_and_note.split('\n',1)
                except ValueError:
                    self.back()
                    return
                self.console.print('\n'+note+'\n')
                self.options = OrderedDict((
                    ('e','edit'),
                    ('r','rename'),
                    ('d','delete'),
                    ('b','back'),
                    ('s','spellcheck'),
                    ('S','scratch'),
                    ('l','links'),
                    ('q','quit')
                ))
            else:
                self.console.print('[bold green]Found total:[/]{}\n'.format(len(self.found)))
                if len(self.found) == 0:
                    self.console.print('\n\n  [red]  No result found...![/]\n\n')
                filenotes = ["   ({}) {} {}".format(k,v.replace(self.app.get_suffix(),""), self.RecentNote(os.path.isfile(Path(Path.home(),"Recent",v.replace(self.app.get_suffix(),".lnk"))))) for k,v in self.found.items()]
                self.console.print("\n".join(filenotes))
                print("\n")
                term = self.app.get_terminal() 
                if term != "alacritty":
                    self.options = OrderedDict((
                        ('n','new'),
                        ('~','clipboard'),
                        ('a','all'),
                        ('h','help'),
                        ('c','config'),
                        ('e','editor'),
                        ('q','quit')
                    ))
                else:
                    self.options = OrderedDict((
                        ('n','new'),
                        ('~','clipboard'),
                        ('a','all'),
                        ('h','help'),
                        ('c','config'),
                        ('t',"terminal"),
                        ('e','editor'),
                        ('q','quit')
                    ))
        self.console.print("\n")
        opt_line = '   '.join(['[bold green]{})[/] {}'.format(k,v) for k, v in self.options.items()])
        self.console.print(opt_line,justify="center")
        #print('-'*self.app.get_columns())
        # better full screen

    @classmethod
    def RecentNote(cls,isrecent):
        if isrecent:
            return  "[red] [Recent] [/]"
        else:
            return ""
    def config(self):
        ## open wintermnote.ini
        try:
            call([self.app.get_editor(), str(Path(self.app._project_root,"wintermnote.ini"))])
        except FileNotFoundError:
            call(["notepad.exe", str(Path(self.app._project_root,"wintermnote.ini"))])

    def editor(self):
        os.chdir(self.app.get_storage())
        try:
            call([self.app.get_editor()])
        except FileNotFoundError:
            call(["notepad.exe"])


    def terminal(self):
        try:
            call([self.app.get_editor(),])
        except  FileNotFoundError:
            pass


    def scratch(self):
        ## save to persistent vim scratch buffer

        source_conf = Path(app.get_storage(),self.note)
        dest_conf = app.get_buffer()
        shutil.copyfile(source_conf,dest_conf)

    def clipboard(self):
        # read ditto clipboard db and generate a note
        usedittodb = True
        note_from_clipboard =[]
        try:
            conn = sqlite3.connect(app.get_ditto_db())
        except sqlite3.OperationalError:
            usedittodb = False
            ## fall back to windows clipboard
            note_from_clipboard.append(pyperclip.paste())

        if usedittodb:
            c = conn.cursor()
            c.execute('SELECT ooData FROM Data where strClipBoardFormat="CF_TEXT"')
            data = c.fetchall()
            for row in data:
                note_from_clipboard.append(row[0].decode('utf-8', 'ignore'))

        if note_from_clipboard:
            # important to set encoding utf8
            with open(Path(app.get_storage(),"FromClipboard.txt"),"w",encoding='utf8') as saveclipb:
                saveclipb.write("FromClipboard\n") #title
                saveclipb.write("".join(note_from_clipboard))
        self.note = "FromClipboard.txt"
        self.display()


    def spellcheck(self):
        ## call aspell.exe
        ## copied aspell.exe +dll from /c/msys2/mingw32/bin
        ## copied dictionary  from /c/msys2/mingw32/lib
        ## installed with pacman


        if self.app.lang is None:
            lang="en"
        else:
            lang=self.app.lang

        if self.app.backup=="no":
            call([str(Path(self.app._project_root,"bin\\aspell.exe")),"--encoding=UTF-8","-x","-d",lang,"-c",os.path.join(self.app.get_storage(), self.note)],shell=False)
        else:
            call([str(Path(self.app._project_root,"bin\\aspell.exe")),"--encoding=UTF-8","-d",lang,"-c",os.path.join(self.app.get_storage(), self.note)],shell=False)

    def all(self):
        self.app.set_columns()
        self.note = None
        self.found = OrderedDict(enumerate(self.allnotes.keys()))

    def links(self):
        with open(os.path.join(self.app.get_storage(), self.note),"r",encoding='utf-8') as note:
            urls=OpenProtocol(text=note.read())
            urls.select()

    def edit(self):
        if self.app.external:
            with open(os.path.join(Path.home(),".wintermnote_mr"),"w") as mr:
                mr.write(os.path.join(self.app.get_storage(), self.note))
                sys.exit()
        try:
            call([self.app.get_editor(), os.path.join(self.app.get_storage(), self.note)])
        except FileNotFoundError:
            call(["notepad.exe", os.path.join(self.app.get_storage(), self.note)])

    def rename(self):
        self.app.clr()
        oldfilename = self.note
        filename = self.app.interact('Enter title: ',
                default = oldfilename.replace(self.app.get_suffix(),""),
                options=self.allwords, isafilename = True)
        filename = app.format_filename(filename)
        filepath = os.path.join(self.app.get_storage(), filename)

        while os.path.exists(filepath):
            self.app.clr()
            if oldfilename == filename:
                return
            self.console.print('[orange]Note "{}" already exists![/]'.format(filename))
            filename = self.app.interact(
                'Try another title: ',
                default=filename,
                options=self.allwords,
                isafilename = True
            )
            filepath = os.path.join(self.app.get_storage(), filename)

        ## change the first line title
        oldfilename = os.path.join(self.app.get_storage(), self.note)

        with open(oldfilename, 'r', encoding='utf-8') as oldnote:
            # read a list of lines into data
            oldnote_text = oldnote.readlines()
        # fix title, keep compatible with vim-note
        oldnote_text[0] = filename.replace(self.app.get_suffix(),"\n")

        with open(filepath,"w", encoding='utf-8') as newnote:
            newnote.writelines(oldnote_text)

        try:
            os.remove(oldfilename)
        except FileNotFoundError:
            print("Error ! Can't remove the file \r\n")
            os.system("pause")
            sys.exit()

        self.scandir()
        for k,v in self.found.items():
            if v == self.note:
                self.found[k] = filename
                break
        self.note = filename

    def delete(self):
        self.app.clr()
        ans = self.app.interact('Delete note "{}"? [ y/N ]: '.format(self.note),
                        options=['y', 'n'], isafilename = False)
        self.app.clr()
        if ans in ['y', 'Y']:
            filepath = os.path.join(self.app.get_storage(), self.note)
            os.remove(filepath)
            if not os.path.isfile(filepath):
                print('Deleted file: "{}"'.format(filepath))
                self.all()
                os.system("pause")
            else:
                print('File not deleted...!')

    def back(self):
        if len(self.found) == 0:
            self.all()
        self.note = None

    def quit(self):
        if self.app.external:
            with open(os.path.join(Path.home(),".wintermnote_mr"),"w") as mr:
                mr.write("")
                sys.exit()
        sys.exit()

    def help(self):
        print()
        print("\nv. {} ".format(WINTERMNOTE_VERSION))
        print("\n TermNote for Windows +Alacritty terminal +GNU/Aspell 0.60 and Ditto clipboard manager.")
        print("\n Author  @seve_py")
        print("\n ===== Configuration =====")
        print("\n Available terminal options \n - default : Command Prompt \n - alacritty : Alacritty\n - wt : Windows Terminal")
        print("\n Default note text file: path relative to user's home dir.")
        print("\n Text editor: Abs exe path if not in the %PATH% e.g. C:\\Users\\user\\Vim\\vim90\\vim.exe ")
        print("\n ===== Commands =====")
        print("\n l - links : open http/https,ftps,mailto links.")

        print("\n S - scratch : save to VIM scratch buffer")

        print("\n ~ - clipboard :  create note from clipboard ditto's history or current clipboard")
        print("\n ")
        os.system("pause")
        return


def runapp(app):
    termnote = TermNote(app)
    # try to set tab name for wt only
    try:
        run(["TITLE","WINTERMNOTE"],shell=True)
    except CalledProcessError as e:
        pass

    ## create/update index
    ud=[]
    ud.append(str(app.get_storage()))
    index = NotesIndex(
                database_file = os.path.join(app.get_storage(),"index.pickle"),
                user_directories = ud,
                character_encoding = 'utf-8',
                case_sensitive = True,
                keyword_filter = None
                )
    app.set_index(index)


    if len(sys.argv) > 1:
        if sys.argv[1:] == ["--external"]:
            app.set_external(True)
            termnote.scandir()
            termnote.all()
        else:
            print(sys.argv[1:])
            searched = ' '.join(sys.argv[1:])
            termnote.search_or_select(searched)
    else:
        termnote.scandir()
        termnote.all()
    while True:
        termnote.display()
        termnote.search_or_select()


if __name__ == '__main__':

    # read config file
    project_root = Path(__file__).parent
    config = configparser.ConfigParser()
    # pdb.set_trace()
    config.read(Path(project_root,"wintermnote.ini"))
    console = Console()

    if config:
        base = config["base"]
        aspell = config["Aspell"]
        ui = config['UI']
        ditto = config['Ditto']
        vim_servername = config['Vim']
        app = App(editor = base["editor"], storage = base["notes_directory"],
                suffix = base["notes_suffix"],
                screen_width = console.size.width,
                lang = aspell["lang"],
                terminal = ui["terminal"],
                backup = aspell["backup"],
                buffer = base["buffer"],
                ditto_db = ditto["clipboard_db"],
                default_notes_file = base["default_notes_file"],
                servername = vim_servername["servername"],
                console = console,
                external = False)
        app.init_storage()
    else:
        raise OSError

    try:
        runapp(app)
    except KeyboardInterrupt:
        runapp(app)
