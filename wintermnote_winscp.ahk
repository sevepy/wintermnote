
; startup script winscp
; author : @seve_py
winscp_installed := GetConfig("winscp","winscp","no")
if (winscp_installed="yes")
{

protocol = %1%
EnvGet, vUserProfile, USERPROFILE
cl := vUserProfile . "\AppData\Local\Programs\WinSCP\winscp.exe " . protocol
Run %cl%
Sleep, 100

If WinExist( "WinSCP" ) ; 
{
    WinActivate , WinSCP
    WinWaitActive , WinSCP
}
}
else {
Msgbox, WinSCP not enabled
}
return


GetConfig(section, key, default) {
    IniRead value, %A_ScriptDir%\wintermnote.ini, %section%, %key%, %default%
    return value
}

