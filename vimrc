let g:netrw_banner = 0
let g:netrw_list_hide = '\=/\=$,tags,.pickle,.ico,.ini,.vimrc,.bib'
let g:netrw_hide = 1
let g:netrw_browse_split = 2
let g:netrw_altv = 1
let g:netrw_liststyle = 4
let g:netrw_menu =1
let g:netrw_winsize = 40
"netrw will keep the browsing directory the same as the current directory.
let g:netrw_keepdir= 0

" please no folding
set nofoldenable
let g:session_autoload = 'no'  


let g:homedirw32 = fnamemodify('~',':p')

" Grepper conf
let g:grepper = {
 \ 'rg': {
            \ 'grepprg':  g:homedirw32.  '\\bin\\rg.exe -H --no-heading --vimgrep',
            \ 'grepformat': '%f:%l:%c:%m',
            \ 'escape':     '\^$.*+?()[]{}|'}
        \}   
let g:grepper.dir = 'cwd'
let g:grepper.tools = ['rg','git','grep']
let g:grepper.jump = 0
nnoremap <leader>g :Grepper<cr>
let g:grepper.prompt_mapping_tool = '<leader>g'
let g:grepper.stop = 500


"" CTRLP config
"" don't show files when :SearchNotes
set wildignore+=*.pickle,tags,*.ctags,notetags,*.ini

set autochdir
let g:notes_tagsindex="tags"

"" CTRLP tags fixed for vim-notes
set tags=notetags
set autochdir

if has('win32') || has('win64')
let g:ctrlp_user_command = 'dir %s /-n /b /s /a-d'  " Windows
"" fix colorscheme - alacritty terminal
set termguicolors


endif


"let g:ctrlp_custom_ignore = {
  "\ 'dir':  '\v[\/]\.(git|hg|svn)$',
  "\ 'file': '.*',
  "\ 'link': '\v(.*)',
  "\ }

let g:ctrlp_show_hidden = 0
let g:ctrlp_use_readdir = 1
augroup notes
    au BufWritePost,VimEnter  *.txt 
                \   silent execute '!'.g:homedirw32.'\\bin\\utags.exe -R -f notetags --options='.expand("%:p:h").'\\Notes.ctags' 
augroup END
