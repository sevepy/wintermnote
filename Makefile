
clean:
	rm -rf build
	rm -rf output
	rm -rf dist
	rm wintermnote.exe
	rm wintermnote_winscp.exe

win32:
	start wintermnote_Compile.ahk
	start wintermnote_winscp_Compile.ahk
	pyinstaller --noconfirm --onedir --console --icon "C:/Users/Seve/workplace/wintermnote/iconified/favicon.ico" --add-data "C:/Users/Seve/workplace/wintermnote/desktop.ini;."  --add-data "C:/Users/Seve/workplace/wintermnote/open_cbthunderlink.exe;."   --add-data "C:/Users/Seve/workplace/wintermnote/wintermnote.bat;."   --exclude-module "scipy" --add-data "C:/Users/Seve/workplace/wintermnote/wintermnote.ico;." --add-data "C:/Users/Seve/workplace/wintermnote/wintermnote.ini;." --exclude-module "zope" --exclude-module "PIL" --exclude-module "matplotlib" --add-data "C:/Users/Seve/workplace/wintermnote/vimrc;." --add-data "C:/Users/Seve/workplace/wintermnote/wintermnote_winscp.exe;." --add-data "C:/Users/Seve/workplace/wintermnote/wintermnote.exe;." --add-data "C:/Users/Seve/workplace/wintermnote/.alacritty.yml;." --add-data "C:/Users/Seve/workplace/wintermnote/alacritty.exe;." --add-data "C:/Users/Seve/workplace/wintermnote/bin;bin"  --add-data "C:/Users/Seve/workplace/wintermnote/lib;lib"  "C:/Users/Seve/workplace/wintermnote/wintermnote_terminal.py"
	cp wintermnote.exe ./dist/wintermnote_terminal/
tags:
	ctags.exe --recurse=yes --exclude=.git
